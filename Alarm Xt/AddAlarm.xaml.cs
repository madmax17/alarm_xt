﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Alarm_Xt
{
    public partial class AddAlarm : PhoneApplicationPage
    {
        public AddAlarm()
        {
            InitializeComponent();
        }
        PhoneApplicationService phoneAppService = PhoneApplicationService.Current;
        DateTime dt;

        private void ApplicationBarCancel(object sender, EventArgs e)
        {
            //Navigate to Settings page
            NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
        }
        
        private void ApplicationBarConfirm(object sender, EventArgs e)
        {
            //dt gets the value of myTimepicker
            dt = (DateTime)myTimePicker.Value;

            //AppService receives dt but only time without the date
            phoneAppService.State["timePicker"] = dt.ToShortTimeString();

            //Navigate to Settings page
            NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
        }

 
    }
}