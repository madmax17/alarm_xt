﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;

namespace Alarm_Xt
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();
            this.listAlarmPicker.ItemsSource = new List<string>() { "Alarm_01", "Alarm_02", "Alarm_03", "Alarm_04" };
        }
        PhoneApplicationService phoneAppService = PhoneApplicationService.Current;
        bool focused = false;

        private void ApplicationBarNewAlarm(object sender, EventArgs e)
        {
            //Navigate to Add Alarm page
            NavigationService.Navigate(new Uri("/AddAlarm.xaml", UriKind.Relative));
        }

        private void ApplicationBarMenuItemAbout(object sender, EventArgs e)
        {
            //Navigate to About page
            NavigationService.Navigate(new Uri("/about.xaml", UriKind.Relative));
        }

        private void ApplicationBarHome(object sender, EventArgs e)
        {
            //Navigate to Main page
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }

        //when the page is loaded event
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            object alarm;

            //if key exists for Time
            if (phoneAppService.State.ContainsKey("timePicker"))
            {
                if (phoneAppService.State.TryGetValue("timePicker", out alarm))
                    textAlarm.Text = alarm.ToString();
            }

        }
        //on navigated from save alarm sound into key
        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            phoneAppService.State["alarmSound"] = listAlarmPicker.SelectedItem.ToString();
            base.OnNavigatedFrom(e);
        }

        //on navigated to assign key back into listAlarmPicker
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            object soundName;

            //if key alarmSound exists assign it back to listAlarmPicker
            if (phoneAppService.State.ContainsKey("alarmSound"))
            {
                if (phoneAppService.State.TryGetValue("alarmSound", out soundName))
                    listAlarmPicker.SelectedItem = soundName.ToString();
            }

            base.OnNavigatedTo(e);
        }

        private void listAlarmPicker_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (focused)
            {
                //add currently selected item in ListPicker to fileName
                string fileName = listAlarmPicker.SelectedItem.ToString();

                string path = null;
                path = "/Alarm XT;component/Sound/" + fileName + ".wav";

                //load sound via resource stream and uri
                var alarm_sound = Application.GetResourceStream(new Uri(path, UriKind.Relative));
                //if sound is loaded
                if (alarm_sound != null)
                {   //load sound using SoundEffect XNA
                    var effect = SoundEffect.FromStream(alarm_sound.Stream);
                    //create instance
                    SoundEffectInstance effectInstance = effect.CreateInstance();
                    //don't loop
                    effectInstance.IsLooped = false;
                    //recommended for XNA&Sivlerlight
                    FrameworkDispatcher.Update();
                    //play the sound
                    effectInstance.Play();
                }
            }
        }

        private void listAlarmPicker_LostFocus(object sender, RoutedEventArgs e)
        {
            focused = false;
        }

        private void listAlarmPicker_ManipulationStarted(object sender, ManipulationStartedEventArgs e)
        {
            focused = true;
        }


    }
}