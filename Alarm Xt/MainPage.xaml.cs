﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Phone.Shell;
using System.Windows.Threading;
using Microsoft.Phone.Info;

namespace Alarm_Xt
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            StartClock();
            dispatcherTimer = new DispatcherTimer();
            dispatcherTimer.Tick += dispatcherTick;
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            dispatcherTimer.Start();
        }
        PhoneApplicationService phoneAppService = PhoneApplicationService.Current;
        SoundEffectInstance effectInstance;
        DispatcherTimer dispatcherTimer;
        bool alarmPlayed = false;

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            kickoff_alarm();
        }

        private void StartClock()
        {
            
            System.DateTime currentDate = DateTime.Now;
            textBoxDate.Text = currentDate.ToShortDateString();

            double hourangle = (((float)currentDate.Hour) / 12) * 360 + currentDate.Minute / 2;
            double minangle = (((float)currentDate.Minute) / 60) * 360;
            double secangle = (((float)currentDate.Second) / 60) * 360;

            hourAnimation.From = hourangle;

            hourAnimation.To = hourangle + 360;

            minuteAnimation.From = minangle;
            minuteAnimation.To = minangle + 360;

            secondAnimation.From = secangle;
            secondAnimation.To = secangle + 360;

            storyboardClock.Begin();
        }

        private void ButtonSettings_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //Navigate to new page
            NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative));
        }

        void play_Alarm()
        {
            object soundName;

            //default alarm sound
            string fileName = "Alarm_01";

            //if key alarmSound exists assign it to filename
            if (phoneAppService.State.ContainsKey("alarmSound"))
            {
                if (phoneAppService.State.TryGetValue("alarmSound", out soundName))
                    fileName = soundName.ToString();
            }
            
            string path = null;
            path = "/Alarm XT;component/Sound/"+fileName+".wav";

            //load sound via resource stream and uri
            var alarm_sound = Application.GetResourceStream(new Uri(path, UriKind.Relative));
            //if sound is loaded
            if ((alarm_sound != null)&&!alarmPlayed)
            {   //load sound using SoundEffect XNA
                var effect = SoundEffect.FromStream(alarm_sound.Stream);
                //create instance
                effectInstance = effect.CreateInstance();
                //loop the sound
                effectInstance.IsLooped = true;
                //recommended for XNA&Sivlerlight
                FrameworkDispatcher.Update();
                //play the sound
                effectInstance.Play();
                alarmPlayed = true;
            }
        }

        private void kickoff_alarm()
        {
            object alarm;

            //if key exists for Time
            if (phoneAppService.State.ContainsKey("timePicker"))
            {
                if (phoneAppService.State.TryGetValue("timePicker", out alarm))
                    textBlockAlarm.Text = alarm.ToString();
                //make the bell visible
                imageBell.Visibility = System.Windows.Visibility.Visible;
            }

            else
                //hide the bell image
                imageBell.Visibility = System.Windows.Visibility.Collapsed;

        }

        private void dispatcherTick(object sender, EventArgs e)
        {
            DateTime dat = DateTime.Now;
            object alarm;

            //if key exists for Time
            if (phoneAppService.State.ContainsKey("timePicker"))
            {
                if (phoneAppService.State.TryGetValue("timePicker", out alarm))
                {
                    //if alarm is equal to present time
                    if ((dat.ToShortTimeString() == alarm.ToString()) && !alarmPlayed)
                    {
                        //play alarm
                        play_Alarm();
                        //stop dispatcher
                        dispatcherTimer.Stop();
                        //Wake up + username
                        textBlockDialog.Text = "Wake up " + GetWindowsLiveAnonymousID();
                        //canvas is visible
                        canvasDialog.Visibility = System.Windows.Visibility.Visible;
                    }
                }
            }
        }
        //get user ID name
        public static string GetWindowsLiveAnonymousID()
        {
            object anid;
            int ANIDLength = 32;
            int ANIDOffset = 2;

            if (UserExtendedProperties.TryGetValue("ANID", out anid))
            {
                if (anid != null && anid.ToString().Length >= (ANIDLength + ANIDOffset - 1))
                {
                    return anid.ToString().Substring(ANIDOffset, ANIDLength);
                }
                else
                {
                    return "???";
                }
            }
            else
            {
                return "???";
            }
        }

        private void buttonDialog_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //collapse dialog
            canvasDialog.Visibility = System.Windows.Visibility.Collapsed;
            //stop alarm sound
            effectInstance.Stop();
            //dispose remaining effects
            effectInstance.Dispose();
            dispatcherTimer.Stop();
        }
    }
}